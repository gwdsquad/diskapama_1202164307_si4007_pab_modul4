package com.elder.SyifaArizalluDiskapama_1202164307_si4007_pab_modul4;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;

public class RegisterActivity extends AppCompatActivity {
    EditText nama,email,pass;
FirebaseAuth mAuth;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        nama =  findViewById(R.id.edNameRegist);
        email = findViewById(R.id.edEmailRegist);
        pass = findViewById(R.id.edPassRegist);


    }

    public void regist(View view){
        if (check()){
            Toast.makeText(this, "Signin up..", Toast.LENGTH_SHORT).show();
            mAuth = FirebaseAuth.getInstance();
            mAuth.createUserWithEmailAndPassword(email.getText().toString(),pass.getText().toString())
                    .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()){
                                FirebaseUser user = mAuth.getCurrentUser();
                                UserProfileChangeRequest userProfileChangeRequest = new UserProfileChangeRequest.Builder().setDisplayName(nama.getText().toString()).build();

                                user.updateProfile(userProfileChangeRequest);
                                startActivity(new Intent(RegisterActivity.this,LoginActivity.class));
                                finish();
                            }else{
                                Toast.makeText(RegisterActivity.this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
        }
    }


    public boolean check(){
        if (nama.getText().toString().equals("")){
            nama.setError("Masukkan Namamu");
            nama.requestFocus();
            return false;
        }
        if (email.getText().toString().equals("")){
            email.setError("Masukkan Namamu");
            email.requestFocus();
            return false;
        }

        if (pass.getText().toString().equals("")){
            pass.setError("Masukkan Namamu");
            pass.requestFocus();
            return false;
        }
        return true;
    }


    public void gotoLogin(View view){
        startActivity(new Intent(RegisterActivity.this,LoginActivity.class));
        finish();
    }
}
